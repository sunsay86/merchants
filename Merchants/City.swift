//
//  City.swift
//  Merchants
//
//  Created by Александр Волков on 11.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
import SwiftyJSON

struct City  {
    
    var id: Int
    let regionId: Int           // ид региона
    let name: String            // название
    let timezone: String        // часовой пояс. пример: GMT-8:00
    let version: String         // инкрементная версия, целое число
    let isDeleted: Bool         // отметка об удалении
    let latitude: Double?       // широта
    let longitude: Double?      // долгота
    
    
    
    init?(json: JSON)
    {
        
        self.id = json[CityKey.id].int!
        self.regionId = json[CityKey.regionId].int!
        self.name = json[CityKey.name].string!
        self.timezone = json[CityKey.timezone].string!
        self.version = json[CityKey.version].string!
        self.isDeleted = json[CityKey.isDeleted].bool!
        self.latitude = json[CityKey.latitude].double
        self.longitude = json[CityKey.longitude].double
        
    }
    
    // MARK:  JSON Keys
    struct CityKey {
        
        static let id = "id"
        static let regionId = "region_id"
        static let name = "name"
        static let timezone = "timezone"
        static let version = "version"
        static let isDeleted = "is_deleted"
        static let latitude = "latitude"
        static let longitude = "longitude"
        
    }

}
