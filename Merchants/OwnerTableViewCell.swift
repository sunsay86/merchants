//
//  OwnerTableViewCell.swift
//  Merchants
//
//  Created by Александр Волков on 10.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class OwnerTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: Model
    
    var logo: UIImage?
    var merchant: Merchant? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: Update UI
    private func updateUI() {
        nameLabel?.text = merchant?.name
        descriptionLabel?.text = merchant?.description
        logoImageView.backgroundColor = UIColor.gray
        logoImageView?.image = logo
        
        
    }

}
