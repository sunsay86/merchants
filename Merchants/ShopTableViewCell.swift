//
//  ShopTableViewCell.swift
//  Merchants
//
//  Created by Александр Волков on 11.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    
    // MARK: Model
    
    
    var shop: Shop? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: Update UI
    private func updateUI() {
        addressLabel?.text = shop?.address
        //cityLabel?.text = String(shop?.cityId)
        
        if let cityID =  shop?.cityId {
            
            
            let server = Server()
            
            server.fetchCity(with: cityID) { [weak self] city  in
                self?.cityLabel?.text = city ?? ""
            }
            
        } else {
            self.cityLabel?.text = ""
            
            
        }

        
    }

}
