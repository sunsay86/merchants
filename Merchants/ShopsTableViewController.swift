//
//  ShopsTableViewController.swift
//  Merchants
//
//  Created by Александр Волков on 10.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class ShopsTableViewController: UITableViewController {

    // MARK: Model
    
    var merchant: Merchant? {
        didSet {
            requestShops(with: merchant!.id, count: 100)
        }
    }
    
    var logoImage: UIImage?
    
    private var shops = [[Shop]]()
    
    
    
    // MARK: request shops
    private func requestShops(with merchantID: Int, count: Int) {
        
        let request = Server()
        
        request.fetchStores(withMerchant: merchantID) { [weak self] (shops, serverError, failure)  in
            
            DispatchQueue.main.async {
                var serverAction = ""
                var error = ""
                
                if shops != nil {
                    self?.shops.insert(shops!, at: 0)
                    
                    self?.tableView.insertSections([1], with: .fade)
                    
                }
                
                
                if serverError != nil {
                    error = serverError!.message
                    serverAction = serverError!.action ?? ""
                    
                }
                
                if failure != nil {
                    error = failure!.localizedDescription
                }
                
                if (self != nil) {
                    if serverError != nil || failure != nil {
                        let alert = UIAlertController(title: error, message: serverAction, preferredStyle: UIAlertControllerStyle.alert)
                        
                        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
                        alert.addAction(cancelAction)
                        
                        self?.present(alert, animated: true, completion: nil)
                    }
                    
                }
                self?.refreshControl?.endRefreshing()
            }
            
            
        }
    }
    
    // MARK: Refresh
    @IBAction func refresh(_ sender: UIRefreshControl) {
        requestShops(with: 1, count: merchant!.id)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            return 100
        }
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return shops.count + 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else  {
            return shops[section - 1].count
        }
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.section == 0 {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerCell", for: indexPath)
            if let ownerCell = cell as? OwnerTableViewCell {
                
                ownerCell.logo = logoImage
                ownerCell.merchant = merchant
                
            }

            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShopCell", for: indexPath)
            
            let shop = shops[indexPath.section - 1][indexPath.row]
            
            if let shopCell = cell as? ShopTableViewCell{
               shopCell.shop = shop
            }
            return cell
        }
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
