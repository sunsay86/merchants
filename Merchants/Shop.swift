//
//  Shop.swift
//  Merchants
//
//  Created by Александр Волков on 08.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Shop  {
    
    let id: Int
    let merchantId: Int     // ид мерчанта
    let cityId: Int         // ид города
    let address: String     // адрес
    let latitude: Double?    // широта
    let longitude: Double?   // долгота
    let isMain: Bool        // главный филиал
    let isSameDays: Bool    // использовать number=-1
    let version: String     // инкрементная версия, целое число
    let isDeleted: Bool     // отметка об удалении
    
    init?(json: JSON)
    {
        
        self.id = json[ShopKey.id].int!
        self.merchantId = json[ShopKey.merchantId].int!
        self.cityId = json[ShopKey.cityId].int!
        self.address = json[ShopKey.address].string!
        self.latitude = json[ShopKey.latitude].double
        self.longitude = json[ShopKey.longitude].double
        self.isMain = json[ShopKey.isMain].bool!
        self.isSameDays = json[ShopKey.isSameDays].bool!
        self.version = json[ShopKey.version].string!
        self.isDeleted = json[ShopKey.isDeleted].bool!
    }
    
    // MARK:  JSON Keys
    struct ShopKey {
        
        static let id = "id"
        static let merchantId = "merchant_id"
        static let cityId = "city_id"
        static let address = "address"
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let isMain = "is_main"
        static let isSameDays = "is_same_days"
        static let version = "version"
        static let isDeleted = "is_deleted"
    }

}
