//
//  Server.swift
//  Merchants
//
//  Created by Александр Волков on 08.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Server {
    
    private let baseURL = URL(string: "http://evotor-api.vigroup.ru/v1")!
    
    
    
    
    // MARK: Parse response Merchants
    
    func fetch(merchants count : Int, handler: @escaping ([Merchant]?, ServerError?, Error?)-> Void) {
        
        var merchants: [Merchant]!
        var serverError: ServerError!
        var error: Error!
        
        fetch(GetKey.merchants, count: count, onSuccess: { result in
            
            
            if let list = result[JSONKey.list].array {
                merchants = list.map{
                   
                    let merch = Merchant(json: $0)!
                    print(merch.descriptionString)
                    return merch
                    }.filter{!$0.name.isEmpty}
                
                handler(merchants, nil, nil)
                
            } else {
                
                let message = result[JSONKey.message].string!
                let action: String? = result[JSONKey.action].string
                
                serverError = ServerError(message: message, action: action)
                handler(nil, serverError, nil)
            }
            
        }) { failure in
            print(failure.localizedDescription)
            error = failure
            handler(nil, nil, error)
        }
        
        
    }
    //  MARK: Parse response Stores Name
    func fetchStores(withMerchant id : Int, handler: @escaping ([Shop]?, ServerError?, Error?)-> Void) {
        print("fetch stores with mercant id:\(id)")
        var shops: [Shop]!
        var serverError: ServerError!
        var error: Error!
        
        fetch(GetKey.stores, count: 1000, onSuccess: { result in
            
            
            if let list = result[JSONKey.list].array {
                shops = list
                    .filter{
                        $0["merchant_id"].int! == id}
                    .map{
                    
                    let shop = Shop(json: $0)!
                    print(shop.address)
                    return shop
                    }
                print(shops.count)
                handler(shops, nil, nil)
                
            } else {
                
                let message = result[JSONKey.message].string!
                let action: String? = result[JSONKey.action].string
                
                serverError = ServerError(message: message, action: action)
                handler(nil, serverError, nil)
            }
            
        }) { failure in
            print(failure.localizedDescription)
            error = failure
            handler(nil, nil, error)
        }
        
        
    }

    
    // MARK: Parse response City Name
    
    func fetchCity(with id: Int, handler: @escaping (String?)-> Void){
        var cityName: String?
//        var serverError: ServerError!
//        var error: Error!
        
        fetch(GetKey.cities, count: 100, onSuccess: { result in
          
            
            if let list = result[JSONKey.list].array {
                
               let city = list.filter { $0["id"].int! == id }
                
                cityName = city[0]["name"].string!
               
                
                handler(cityName)
            }
            
//            } else {
//                
//                let message = result[JSONKey.message].string!
//                let action: String? = result[JSONKey.action].string
//                
//                serverError = ServerError(message: message, action: action)
//                handler(nil, serverError, nil)
//            }
            
        }) { failure in
            print(failure.localizedDescription)
//            error = failure
//            handler(nil, nil, error)
        }
    }
    
    
    
    // MARK: GET Request
    
    private func fetch(_ items: String, count: Int,
                       onSuccess: @escaping (JSON) -> Void,
                       onFailure: @escaping (Error) -> Void) {
        
        let json = JSON(["count": count])
        let parameters: [String: Any] = ["no_token" : 1, "data": json]
        
        Alamofire.request("\(baseURL)/\(items)", parameters: parameters).responseJSON { response in
            
            print(String(describing: response.request))
            print(String(describing: response.response))
            
            switch(response.result) {
            case .success(_):
                
                let json = JSON(data: response.data!)
                onSuccess(json)
                
                break
                
            case .failure(_):
                print(response.result.error!)
                
                onFailure(response.result.error!)
                
                break
                
            }
        }
        
    }
    
    // MARK: download Image
    
    func fetchLogo(with url: URL, handler: @escaping (UIImage) -> Void){
        
        request(url).validate().response { response in
            guard
                let data = response.data,
                let image = UIImage(data: data)
                else { return }
            handler(image)
        }
        
    }
    
    
    // MARK: Keys for GET method
    struct GetKey {
        static let merchants = "merchants"
        static let cities = "cities"
        static let stores = "stores"
    }
    
    // MARK: Keys from JSON
    struct JSONKey {
        static let list = "list"
        static let action = "action"
        static let message = "message"
    }
    
    
    
}
