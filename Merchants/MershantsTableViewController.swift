//
//  MershantsTableViewController.swift
//  Merchants
//
//  Created by Александр Волков on 08.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class MershantsTableViewController: UITableViewController {
  
    
    // MARK: Model
  

    private var merchants = [[Merchant]]()
    private var fetchCount = 20
    
    private var server: Server?
    
    // MARK: request merchants
    private func requestMerchants(count: Int) {
        
       let request = Server()
        
            request.fetch(merchants: count) { [weak self] (merchs, serverError, failure)  in
                self?.merchants.removeAll()
                DispatchQueue.main.async {
                    var serverAction = ""
                    var error = ""
                    
                    if merchs != nil {
                        self?.merchants.insert(merchs!, at: 0)
                        self?.tableView.reloadData()

                    }
                    
                    
                    if serverError != nil {
                        error = serverError!.message
                        serverAction = serverError!.action ?? ""
                        
                    }
                    
                    if failure != nil {
                        error = failure!.localizedDescription
                    }
                    
                    if (self != nil) {
                        if serverError != nil || failure != nil {
                            let alert = UIAlertController(title: error, message: serverAction, preferredStyle: UIAlertControllerStyle.alert)
                            
                            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
                            alert.addAction(cancelAction)
                            
                            self?.present(alert, animated: true, completion: nil)
                        }

                    }
                 self?.refreshControl?.endRefreshing()
                }
                
                
        }
    }
    
    // MARK: Refresh
    @IBAction func refresh(_ sender: UIRefreshControl) {
        requestMerchants(count: fetchCount)
    }

    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = tableView.rowHeight
       
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.requestMerchants(count: fetchCount)
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return merchants.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchants[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MerchantCell", for: indexPath)
        
        
        if self.merchants.count != 0 {
            
        
            let merchant = self.merchants[indexPath.section][indexPath.row]
            
            if let merchantCell = cell as? MerchantTableViewCell {
                merchantCell.merchant = merchant
            }
        }
            
        
        
        return cell
    }
   
   // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? MerchantTableViewCell {
            
            if segue.identifier == "showShops" {
                
                let stvc: ShopsTableViewController = segue.destination as! ShopsTableViewController
                
                stvc.merchant = cell.merchant
                stvc.logoImage = cell.logoImageView.image
            }
        }
        
    }
 
}
