//
//  MerchantTableViewCell.swift
//  Merchants
//
//  Created by Александр Волков on 08.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class MerchantTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: Model
    var merchant: Merchant? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: Update UI
    private func updateUI() {
        nameLabel?.text = merchant?.name
        descriptionLabel?.text = merchant?.description
        
        if let logoString =  merchant?.logoUrl {
            let logoURL = URL(string: logoString)!
            let server = Server()
            
            server.fetchLogo(with: logoURL) { [weak self] image in
                self?.logoImageView?.image = image
            }
            
        } else {
            logoImageView?.backgroundColor = UIColor.gray
           
            
        }
    }
    

}
