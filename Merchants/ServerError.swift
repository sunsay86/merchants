//
//  ServerError.swift
//  Merchants
//
//  Created by Александр Волков on 09.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation

struct ServerError {
    let action: String?
    let message: String
    
    init?(message: String, action: String?){
        self.action = action
        self.message = message
    }
}
