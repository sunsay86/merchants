//
//  Merchant.swift
//  Merchants
//
//  Created by Александр Волков on 08.07.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
import SwiftyJSON


struct Merchant {
    
    public let id: Int
    public let name: String                     // название
    public let description: String              // описание
    public let logoUrl: String?                  // логотип
    public let defaultDiscount: Double          // значение скидки по умолчанию
    public let discountDescriptionUrl: String?   // ссылка на условия программы лояльности
    public let version: String                  // инкрементная версия, целое число
    public let isDeleted: Bool                  // отметка об удалении
    public let popular: Int                     // популярность
    public var descriptionString: String {
        return "id = \(id)\n" +
                "name = \(name)\n" +
                "desription = \(description)\n" +
                "logoUrl = \(logoUrl ?? "no URL")"    
    
    }
    
    init?(json: JSON)
    {

        self.id = json[MerchantKey.id].int!
        self.name = json[MerchantKey.name].string!
        self.description = json[MerchantKey.description].string!
        self.logoUrl = json[MerchantKey.logoUrl].string
        self.defaultDiscount = json[MerchantKey.defaultDiscount].double!
        self.discountDescriptionUrl = json[MerchantKey.discountDescriptionUrl].string
        self.version = json[MerchantKey.version].string!
        self.isDeleted = json[MerchantKey.isDeleted].bool!
        self.popular = json[MerchantKey.popular].int!
        
        
    }
    
    // MARK:  JSON Keys
    struct MerchantKey {
        static let list = "list"
        static let id = "id"
        static let name = "name"
        static let description = "description"
        static let logoUrl = "logo_url"
        static let defaultDiscount = "default_discount"
        static let discountDescriptionUrl = "discount_description_url"
        static let version = "version"
        static let isDeleted = "is_deleted"
        static let popular = "popular"
        
    }
}


